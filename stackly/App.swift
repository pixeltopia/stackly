import UIKit
import SnapKit
import LoremSwiftum

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    typealias Options = [UIApplication.LaunchOptionsKey: Any]

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: Options?) -> Bool {
        return true
    }
}

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        LayoutView {
            HBox {
                Slot(h: 50%, v: .matchContents, kind: .kind1, name: .name1) {
                    Text { it in
                        it.text = Lorem.sentences(3)
                        it.font = .preferredFont(forTextStyle: .body)
                        it.backgroundColor = .clear
                    }
                }
                VBox {
                    Slot(v: 25%, kind: .kind2, name: .name2)
                    Slot(v: 20.px, kind: .kind3, name: .name3) {
                        LayoutView {
                            HBox {
                                Slot(h: .ratio(1/3), kind: .kind1, name: .name1)
                                Slot(h: .ratio(1/3), kind: .kind2, name: .name2)
                                Slot(h: .ratio(1/3), kind: .kind3, name: .name3)
                            }
                        }.tap { it in
                            it[.name1]?.backgroundColor = .systemTeal
                            it[.name2]?.backgroundColor = .systemPink
                            it[.name3]?.backgroundColor = .clear
                        }
                    }
                    VBox {
                        Slot(kind: .kind2, name: .name4)
                        Slot(v: 20.px, kind: .kind3, name: .name5)
                    }
                }
            }
        }.tap { it in
            self.view.addSubview(it)
        }.tap { it in
            it[.name1]?.backgroundColor = .red
            it[.name2]?.backgroundColor = .blue
            it[.name3]?.backgroundColor = .green
            it[.name4]?.backgroundColor = .orange
            it[.name5]?.backgroundColor = .yellow
        }.tap { it in
            it.snp.makeConstraints { make in
                make.center.width.equalToSuperview()
            }
        }
    }
}

enum Kind {
    case kind1
    case kind2
    case kind3
}

enum Name {
    case name1
    case name2
    case name3
    case name4
    case name5
    case name6
    case name7
    case name8
}

// MARK:- Text blocks

typealias Text = UITextView

extension Text {
    convenience init(_ block: (Self) -> Void) {
        self.init()
        self.isScrollEnabled = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.tap(block)
    }
}

// MARK:- Tapping into an object

protocol Tappable {}

extension Tappable {
    @discardableResult
    func tap(_ block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
}

extension NSObject: Tappable { }

// MARK:- flex along an axis (horizontal or vertical)

enum Flex {
    case fixed(Double)
    case ratio(Double)
    case flexi
    case matchContents
}

postfix operator %

postfix func %(value: Double) -> Flex {
    .ratio((max(0, min(100, value)) / 100.0).rounded(.tenths))
}

extension Int {
    var px: Flex { .fixed(Double(self)) }
}

extension Double {
    enum Precision: Double {
        case ones = 1.0
        case tenths = 10.0
        case hundredths = 100.0
        case thousandths = 1000.0
    }

    func rounded(_ precision: Precision) -> Self {
        (self * precision.rawValue).rounded() / precision.rawValue
    }
}

// MARK:- A layout returns a tree of nodes

protocol Layout: Accepting {
    @NodeBuilder var nodes: Nodes { get }
}

extension Layout {
    func accept(_ visitor: Visitor) {
        nodes.forEach { it in it.accept(visitor) }
    }
}

typealias Nodes = [Node]

typealias Node = Accepting & Flexy

@_functionBuilder
struct NodeBuilder {
    static func buildBlock(_ nodes: Node...) -> Nodes {
        nodes
    }
}

struct HBox {
    let h: Flex
    let v: Flex
    let nodes: Nodes

    init(h: Flex = .flexi, v: Flex = .flexi, @NodeBuilder nodes: () -> Nodes) {
        self.h = h
        self.v = v
        self.nodes = nodes()
    }
}

struct VBox {
    let h: Flex
    let v: Flex
    let nodes: Nodes

    init(h: Flex = .flexi, v: Flex = .flexi, @NodeBuilder nodes: () -> Nodes) {
        self.h = h
        self.v = v
        self.nodes = nodes()
    }
}

// MARK:- A slot has a kind and a name, and can make a view

struct Slot {

    typealias Make = () -> UIView

    let h: Flex
    let v: Flex
    let kind: Kind
    let name: Name
    let make: Make?

    init(h: Flex = .flexi, v: Flex = .flexi, kind: Kind, name: Name, make: Make? = nil) {
        self.h = h
        self.v = v
        self.kind = kind
        self.name = name
        self.make = make
    }
}

// MARK:- constructing the stack view hierarchy

class LayoutView: UIView {
    private var lookup = [Name: UIView]()
    private var stacks = [UIStackView]()

    subscript(name: Name) -> UIView? {
        lookup[name]
    }

    init(_ layout: Layout) {
        super.init(frame: .zero)
        layout.accept(self)
    }

    init(@NodeBuilder _ nodes: () -> Nodes) {
        super.init(frame: .zero)
        nodes().forEach { it in it.accept(self) }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

extension LayoutView: Visitor {
    func enter(_ hbox: HBox) {
        let view = UIStackView().tap { it in
            it.axis = .horizontal
            it.distribution = .fill
        }

        if let head = stacks.last {
            head.addArrangedSubview(view)
        }

        view.apply(flexy: hbox)

        stacks.append(view)
    }

    func enter(_ vbox: VBox) {
        let view = UIStackView().tap { it in
            it.axis = .vertical
            it.distribution = .fill
        }

        if let head = stacks.last {
            head.addArrangedSubview(view)
        }

        view.apply(flexy: vbox)

        stacks.append(view)
    }

    func enter(_ slot: Slot) {
        guard let head = stacks.last else { return }

        let view = UIView()
        head.addArrangedSubview(view)
        lookup[slot.name] = view

        let contents = slot.make?()
        if let contents = contents {
            view.addSubview(contents)
        }

        view.apply(flexy: slot, contents: contents)
    }

    func leave() {
        if let last = stacks.popLast(), stacks.isEmpty {
            self.addSubview(last)
            last.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        }
    }
}

// MARK:- applying flex constraints

protocol Flexy {
    var h: Flex { get }
    var v: Flex { get }
}

extension HBox: Flexy { }
extension VBox: Flexy { }
extension Slot: Flexy { }

extension UIView {
    func apply(flexy: Flexy, contents: UIView? = nil) {
        switch (flexy.h, superview) {
            case (.ratio(let value), .some):
                self.snp.makeConstraints { make in
                    make.width.equalToSuperview().multipliedBy(value)
                }
                self.snp.contentCompressionResistanceHorizontalPriority = 1000
                self.snp.contentHuggingHorizontalPriority = 1000
            case (.fixed(let value), _):
                self.snp.makeConstraints { make in
                    make.width.equalTo(value)
                }
                self.snp.contentCompressionResistanceHorizontalPriority = 1000
                self.snp.contentHuggingHorizontalPriority = 1000
            case (.flexi, _):
                self.snp.contentCompressionResistanceHorizontalPriority = 0
                self.snp.contentHuggingHorizontalPriority = 0
            case (.matchContents, _):
                self.snp.contentCompressionResistanceHorizontalPriority = 1000
                self.snp.contentHuggingHorizontalPriority = 1000
            case (_, .none):
                break
        }

        if let contents = contents {
            contents.snp.makeConstraints { make in
                make.width.equalToSuperview()
            }
        }

        switch (flexy.v, superview) {
            case (.ratio(let value), .some):
                self.snp.makeConstraints { make in
                    make.height.equalToSuperview().multipliedBy(value)
                }
                self.snp.contentCompressionResistanceVerticalPriority = 1000
                self.snp.contentHuggingVerticalPriority = 1000
            case (.fixed(let value), _):
                self.snp.makeConstraints { make in
                    make.height.equalTo(value)
                }
                self.snp.contentCompressionResistanceVerticalPriority = 1000
                self.snp.contentHuggingVerticalPriority = 1000
            case (.flexi, _):
                self.snp.contentCompressionResistanceVerticalPriority = 0
                self.snp.contentHuggingVerticalPriority = 0
            case (.matchContents, _):
                self.snp.contentCompressionResistanceVerticalPriority = 1000
                self.snp.contentHuggingVerticalPriority = 1000
            case (_, .none):
                break
        }

        if let contents = contents {
            contents.snp.makeConstraints { make in
                make.height.equalToSuperview()
            }
        }
    }
}

// MARK:- iterating down the tree

protocol Visitor {
    func enter(_ hbox: HBox)
    func enter(_ vbox: VBox)
    func enter(_ slot: Slot)
    func leave()
}

protocol Accepting {
    func accept(_ visitor: Visitor)
}

extension HBox: Accepting {
    func accept(_ visitor: Visitor) {
        visitor.enter(self)
        nodes.forEach { it in it.accept(visitor) }
        visitor.leave()
    }
}

extension VBox: Accepting {
    func accept(_ visitor: Visitor) {
        visitor.enter(self)
        nodes.forEach { it in it.accept(visitor) }
        visitor.leave()
    }
}

extension Slot: Accepting {
    func accept(_ visitor: Visitor) {
        visitor.enter(self)
    }
}
